;   T�TULO: PLANTILLA
;   DESCRIPCI�N: PRIMER C�DIGO EN ENSAMBLADOR
;   AUTOR:  DAN
;   CONSIDERACIONES ESPECIALES: NO
    list p=16f877A	    ;Indica el tipo de procesador utilizado
    INCLUDE "P16F877A.INC"  ;Incluye en el progrma el fichero de definiones del uC seleccionado
    __CONFIG _CP_OFF& _DEBUG_OFF& _WRT_OFF& _CPD_OFF& _LVP_OFF& _BODEN_OFF& _PWRTE_ON& _WDT_OFF& _XT_OSC ;selecciona el estado de los bits de configuraci�n
Tiempo1	    equ 0x20    ;Etiqueta para el retardo
Tiempo2	    equ 0x21    ;Etiqueta para el retardo
NVueltas    equ 0x22	;Numero de vueltas
Numero1	    equ 0x23	;Numero 1 del puerto de entrada
Numero2	    equ 0x24	;Numero 2 del puerto de entrada
VAux	    equ 0x25	;Auxiliar almacena el valor
SAux	    equ 0x26	;Auxiliar del sentido
	    
    org 00		;
    
    ;BSF	STATUS,RP0	;BANCO 0
    ;CLRF PORTB		;SALIDA PORT B
    ;BCF	STATUS,RP0	;BANCO 0
    ;MOVLW 0xFF		;BITS COMO ENTRADA 1111111
    ;MOVWF TRISD		;ENTRADA PORT D

INICIO
    banksel TRISA  
    CLRF TRISB ;Puerta B se configura como salida
    MOVLW 0xFF
    MOVWF TRISD;entrada
    BANKSEL PORTB
    CLRF PORTB  ;limpia registro portB en salida
    CLRF Numero1
    CLRF Numero2
    ;asignamos mascaras
    MOVLW 0XF0
    MOVWF Numero1
    MOVLW 0X0F
    MOVWF Numero2
    ;leemos puerto D
    MOVF PORTD,0
    ANDWF Numero1,1
    ;RRF Numero1,1
    ;RRF Numero1,1
    ;RRF Numero1,1
    ;RRF Numero1,1
    SWAPF Numero1,1
    ANDWF Numero2,1
    
    
    
    
    MOVLW .0
    MOVWF SAux	;
    ;MOVLW .3
    ;MOVWF Numero1
    ;MOVLW .3
    ;MOVWF Numero2
    CALL DEFINE_SENTIDO;PRUEBA
    CALL COMPARA
    CALL VUELTAS
    GOTO IGUAL_SIN_GIRO
    GOTO INICIO

DEFINE_SENTIDO
    BCF STATUS,Z
    BCF STATUS,C
    MOVF Numero1,0
    SUBWF Numero2,0
    BTFSS STATUS,C
    CALL SENTIDO_1	;MAYOR NUM 1
    RETURN
    
SENTIDO_1
    MOVLW .1
    MOVWF SAux
    RETURN
    
COMPARA
    BCF STATUS,Z
    BCF STATUS,C
    MOVF Numero1,0
    SUBWF Numero2,0
    BTFSC STATUS,Z
    GOTO IGUAL_SIN_GIRO
    BTFSS STATUS,C
    CALL MAYOR ;MAYOR NUM 1
    GOTO RESTA
    RETURN

MAYOR
    MOVF Numero1,0
    MOVWF VAux
    MOVF Numero2,0
    MOVWF Numero1
    MOVF VAux,0
    MOVWF Numero2
    RETURN

RESTA
   MOVF Numero1,0
   SUBWF Numero2,0
   MOVWF NVueltas
   RETURN 
    
    
    
VUELTAS
    INCF NVueltas,F
    GOTO DECREMENTO_GIRO
    
DECREMENTO_GIRO
    DECFSZ NVueltas,1
    GOTO GIROS_LOCOS
    GOTO IGUAL_SIN_GIRO
    
GIROS_LOCOS
    BCF STATUS,Z
    BCF STATUS,C
    MOVF .0,0
    SUBWF SAux,0
    BTFSC STATUS,Z
    GOTO MENOR_GIRO_RELOJ_IZQUIERDA
    GOTO MAYOR_GIRO_RELOJ_DERECHA
    
    ;GOTO IGUAL_SIN_GIRO



IGUAL_SIN_GIRO
    MOVLW   B'00111111'	; inicio sentido manecillas del reloj
    MOVWF   PORTB
    CALL    DELAY
    GOTO INICIO
    ;GOTO DECREMENTO_GIRO;este se deberia de ir a leer de nuevo a incio o leer
    
MAYOR_GIRO_RELOJ_DERECHA
    MOVLW   B'10000001'	; inicio sentido manecillas del reloj
    MOVWF   PORTB
    CALL    DELAY
    MOVLW   B'00000010'	;
    MOVWF   PORTB
    CALL    DELAY
    MOVLW   B'00000100'	;
    MOVWF   PORTB
    CALL    DELAY
    MOVLW   B'00001000'	;
    MOVWF   PORTB
    CALL    DELAY
    MOVLW   B'00010000'	;
    MOVWF   PORTB
    CALL    DELAY
    MOVLW   B'00100000'	;
    MOVWF   PORTB
    CALL    DELAY
    GOTO DECREMENTO_GIRO
MENOR_GIRO_RELOJ_IZQUIERDA
    MOVLW   B'10000001'	; inicio sentido manecillas del reloj
    MOVWF   PORTB
    CALL    DELAY
    MOVLW   B'00100000'	;
    MOVWF   PORTB
    CALL    DELAY
    MOVLW   B'00010000'	;
    MOVWF   PORTB
    CALL    DELAY
    MOVLW   B'00001000'	;
    MOVWF   PORTB
    CALL    DELAY
    MOVLW   B'00000100'	;
    MOVWF   PORTB
    CALL    DELAY
    MOVLW   B'00000010'	;
    MOVWF   PORTB
    CALL    DELAY
    GOTO DECREMENTO_GIRO
;************************   MODULO DE RETARDO   ******************************
DELAY2
    MOVLW 0XFF
    MOVWF Tiempo1   
DELAY
    DECFSZ Tiempo1,1
    GOTO DELAY
    DECFSZ Tiempo2,1
    GOTO DELAY2
    
    END