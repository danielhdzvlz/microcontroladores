  list p=16f887
    INCLUDE "p16f887.inc"


    __CONFIG    _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
    __CONFIG    _CONFIG2, _WRT_OFF & _BOR21V

    tiem equ 0x20
    tiemp2 equ 0x21
    tiemp3 equ 0x22
    cont equ 0x23
    num1 equ 0x24
    num2 equ 0x25

    org 00
LEER
   banksel TRISA  
	CLRF TRISA	;Puerta A se configura como salida
    CLRF TRISB ;Puerta B se configura como salida
    MOVLW 0xFF
    MOVWF TRISD;entrada

    BANKSEL PORTB
    CLRF PORTA	;limpia registro portA en salida
    CLRF PORTB  ;limpia registro portB en salida
    CLRF num1
    CLRF num2
    ;asignamos mascaras
    MOVLW b'11110000'
    MOVWF num1
    MOVLW b'00001111'
    MOVWF num2
    ;leemos puerto D
    MOVF PORTD,0
    ANDWF num1,1
    RRF num1,1
    RRF num1,1
    RRF num1,1
    RRF num1,1
    ANDWF num2,1
   ;----
    BSF STATUS,C  ;Inicializa las banderas del registro STATUS bit c en 1, --> bit set file --bsf status,0
    BCF STATUS,Z  ;bit clear file, bit numero 2, 3ro de derecha a izquierda en registro status ----> z
                  ; si z=1 , num1 y num2 son iguales
    MOVF num1,0  ; guarda el numero 1 en w
    subwf num2,0   ;(num1-num2) para comprobar mayor, menor o igual, 0 por lo tanto se guarda en w
    ; comprobamos bits de registro status z,c
    btfsc    STATUS,Z    ;Compara la igualdad, sino es igual realiza la otra comparación
                        ;si z=1 siguiente instruccion es ejecutada, z=0 salta instruccion
    goto    igual             ;salta a la rutina "igual"

    btfss    STATUS,C ;Comprueba si num1 es mayor que num2 , si c=0 siguiente instruccion es ejecutada
    goto    mayor          ;Si es menor ejecuta rutina "menor"

    btfsc    STATUS,C ;comprueba si c=0, num1>num2, se ejecuta la siguiente instruccion
    goto    menor
    goto    LEER


;--------------------------
igual
    BCF STATUS,Z
    CLRF	cont		; pone el contador a 0
	MOVF	cont,W		; pasa el contador a w (índice)
	CALL	tabla1		; llama a la tabla
	MOVWF	PORTB	; pasa el dato obtenido a PORTB
	CALL	RETARDO
disp_1
    MOVF	cont,W
	XORLW	B'0001'		; verifica si el contador llegó a 9
	BTFSC	STATUS,Z		; si no es así salta una línea
	GOTO	LEER		; si llegó a 1 lo atiende en reini
	INCF	cont,F		; incrementa el contador
	MOVF	cont,W		; pasa el contador a w (índice)
	CALL	tabla1		; llama a la tabla
	MOVWF	PORTB		; pasa el dato obtenido en la tabla a PORTB
	CALL	RETARDO
	GOTO	disp_1


;--------------- Tabla numeros iguales --------------------

tabla1	ADDWF	PCL,F		; se incrementa el contador de programa
	;display	   . gfedcba	segmentos de los leds del display
             ;pGFEDCBA
	RETLW	B'00111111'	; código para el 0
    RETLW	B'00000000'	; apaga display

;-----------------------------
menor
    BCF STATUS,Z
    CLRF	cont		; pone el contador a 0
	MOVF	cont,W		; pasa el contador a w (índice)
	CALL	tabla2		; llama a la tabla
	MOVWF	PORTB	; pasa el dato obtenido a PORTB
	CALL	RETARDO
disp_2
    MOVF	cont,W
	XORLW	B'0101'		; verifica si el contador llegó a 9
	BTFSC	STATUS,Z	; si no es así salta una línea, si es =1 executa la siguiente instruccion
	GOTO	LEER		; si llegó a 1 lo atiende en reini
	INCF	cont,F		; incrementa el contador
	MOVF	cont,W		; pasa el contador a w (índice)
	CALL	tabla2		; llama a la tabla
	MOVWF	PORTB		; pasa el dato obtenido en la tabla a PORTB
	CALL	RETARDO
	GOTO	disp_2


;--------------- Tabla numero menor --------------------

tabla2	ADDWF	PCL,F		; se incrementa el contador de programa
	;display	   . gfedcba	segmentos de los leds del display
             ;pGFEDCBA
	RETLW	B'00100000'	;inicio en sentido contrario de las manecillas
    RETLW	B'00010000'	;
    RETLW	B'00001000'	;
    RETLW	B'00000100'	;
    RETLW	B'00000010'	;
    RETLW	B'00000001'	;


;-----------------------------
mayor
    BCF STATUS,Z
    CLRF	cont		; pone el contador a 0
	MOVF	cont,W		; pasa el contador a w (índice)
	CALL	tabla3		; llama a la tabla
	MOVWF	PORTB	; pasa el dato obtenido a PORTB
	CALL	RETARDO
disp_3
    MOVF	cont,W
	XORLW	B'0101'		; verifica si el contador llegó a 9
	BTFSC	STATUS,Z		; si no es así salta una línea
	GOTO	LEER		; si llegó a 1 lo atiende en reini
	INCF	cont,F		; incrementa el contador
	MOVF	cont,W		; pasa el contador a w (índice)
	CALL	tabla3		; llama a la tabla
	MOVWF	PORTB		; pasa el dato obtenido en la tabla a PORTB
	CALL	RETARDO
	GOTO	disp_3


;--------------- Tabla numero mayor --------------------

tabla3	ADDWF	PCL,F		; se incrementa el contador de programa
	;display	   . gfedcba	segmentos de los leds del display
             ;pGFEDCBA
	RETLW	B'00000001'	; inicio sentido manecillas del reloj
	RETLW	B'00000010'	;
	RETLW	B'00000100'	;
	RETLW	B'00001000'	;
	RETLW	B'00010000'	;
    RETLW	B'00100000'	;




;-----------Rutina de Retardo-----------

RETARDO
    MOVLW D'4'  ; CARGA W CON 255
    MOVWF tiem   ;tiempo=255
    dec
    DECFSZ tiem
    GOTO RETARDO2
    return

RETARDO2
    MOVLW D'217'
    MOVWF tiemp2
dec2
    DECFSZ tiemp2
    goto RETARDO3
    goto dec

RETARDO3
    MOVLW D'255'
    MOVWF tiemp3
dec3
    DECFSZ tiemp3
    goto dec3
    goto dec2

    END




