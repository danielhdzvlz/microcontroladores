
    list p=16f887
    INCLUDE "p16f887.inc"
    __CONFIG    _CONFIG1, _LVP_OFF & _FCMEN_ON & _IESO_OFF & _BOR_OFF & _CPD_OFF & _CP_OFF & _MCLRE_ON & _PWRTE_ON & _WDT_OFF & _INTRC_OSC_NOCLKOUT
    __CONFIG    _CONFIG2, _WRT_OFF & _BOR21V

    numUno equ 0x21
    numDos equ 0x22
    aux equ 0x23
    tiem equ 0x24
    tiemp2 equ 0x25
    tiemp3 equ 0x26
    org 00

MAIN
    banksel TRISA
	CLRF TRISA	;Puerta A se configura como salida
    CLRF TRISB
    CLRF TRISC

    MOVLW 0xFF
    MOVWF TRISD

    BANKSEL PORTB
    CLRF PORTA	;vuelca el registro portA en salida
    CLRF PORTB
    CLRF PORTC
    ;configuracion de variables

LEER
    CLRF numUno
    CLRF numDos
    CLRF aux

    MOVLW 0xF0
    MOVWF numUno
    MOVLW 0x0F
    MOVWF numDos

    ;MOVF PORTD,0
    MOVLW B'01110011'

    ANDWF numUno,1
    RRF numUno,1
    RRF numUno,1
    RRF numUno,1
    RRF numUno,1
    ANDWF numDos,1
    BCF STATUS,Z
    BCF STATUS,C

    CALL COMPARA
    GOTO LEER

COMPARA
    BCF STATUS,Z
    BCF STATUS,C
    MOVF numUno,0
    SUBWF numDos,0
    BTFSC STATUS,Z
    GOTO IGUAL
    BTFSS STATUS,C
    CALL MAYOR ;MAYOR NUM 1
    GOTO RESTA
    RETURN

IGUAL
    MOVLW .0
    CALL TABLA
    MOVWF PORTB
    GOTO LEER

MAYOR
    MOVF numUno,0
    MOVWF aux
    MOVF numDos,0
    MOVWF numUno
    MOVF aux,0
    MOVWF numDos
    RETURN

RESTA
   MOVF numUno,0
   SUBWF numDos,0
   CALL TABLA
   MOVWF PORTB
   GOTO LEER


TABLA
    ADDWF PCL,1
    RETLW B'00111111'
    RETLW B'00000110'
    RETLW B'01011011'
    RETLW B'01001111'
    RETLW B'01100110'
    RETLW B'01101101'
    RETLW B'01111101'
    RETLW B'00000111'
    RETLW B'01111111'
    RETLW B'01100111'
    RETLW B'11110111';A
    RETLW B'11111111';B
    RETLW B'10111001';C
    RETLW B'10111111';D
    RETLW B'11111001';E
    RETLW B'11110001';F


END






