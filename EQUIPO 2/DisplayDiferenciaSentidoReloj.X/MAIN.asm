;   T�TULO: PLANTILLA
;   DESCRIPCI�N: PRIMER C�DIGO EN ENSAMBLADOR
;   AUTOR:  DAN
;   CONSIDERACIONES ESPECIALES: NO
    list p=16f877A	    ;Indica el tipo de procesador utilizado
    INCLUDE "P16F877A.INC"  ;Incluye en el progrma el fichero de definiones del uC seleccionado
    __CONFIG _CP_OFF& _DEBUG_OFF& _WRT_OFF& _CPD_OFF& _LVP_OFF& _BODEN_OFF& _PWRTE_ON& _WDT_OFF& _XT_OSC ;selecciona el estado de los bits de configuraci�n
 
tiem equ 0x20
tiemp2 equ 0x21
tiemp3 equ 0x21
cont equ 0x23
num1 equ 0x24
num2 equ 0x25
aux equ 0x26
diferencia equ 0x27

    org 00
LEER
    banksel TRISA  
    CLRF TRISA	;Puerta A se configura como salida
    CLRF TRISB ;Puerta B se configura como salida
    MOVLW 0xFF
    MOVWF TRISD;entrada

    BANKSEL PORTB
    CLRF PORTA	;limpia registro portA en salida
    CLRF PORTB  ;limpia registro portB en salida
    CLRF num1
    CLRF num2
    CLRF aux
    ;asignamos mascaras
    MOVLW b'11110000'
    MOVWF num1
    MOVLW b'00001111'
    MOVWF num2
    ;leemos puerto D
    MOVF PORTD,0
    ANDWF num1,1
    RRF num1,1
    RRF num1,1
    RRF num1,1
    RRF num1,1
    ANDWF num2,1
   ;----
    BSF STATUS,C  ;
    BCF STATUS,Z  ;
    MOVF num1,0  ; guarda el numero 1 en w
    subwf num2,0   ;(num1-num2) para comprobar mayor, menor o igual, 0 por lo tanto se guarda en w
    btfsc    STATUS,Z    ;Compara la igualdad, sino es igual realiza la otra comparaci�n
    goto    igual             ;salta a la rutina "igual"
    BTFSS STATUS,C
    CALL VerificarMayor ;MAYOR NUM 1
    CALL RESTA
    
    ;---------------------------------------------  
    MOVF num1,0  ; guarda el numero 1 en w
    subwf num2,0   ;(num1-num2) para comprobar mayor, menor o igual, 0 por lo tanto se guarda en w
    ; comprobamos bits de registro status z,c
    btfsc    STATUS,Z    ;Compara la igualdad, sino es igual realiza la otra comparaci�n
    GOTO    IGUAL2            ;salta a la rutina "igual"
    btfss    STATUS,C ;Comprueba si num1 es mayor que num2 , si c=0 siguiente instruccion es ejecutada
    goto    CicloMayor          ;Si es menor ejecuta rutina "menor"
    btfsc    STATUS,C ;comprueba si c=0, num1>num2, se ejecuta la siguiente instruccion
    goto    CicloMenor
    goto    LEER

CicloMayor
    DECFSZ diferencia,1
    
    GOTO LEER
CicloMenor
    DECFSZ diferencia,1
    call mayor
    GOTO LEER
igual
    nop
    return
VerificarMayor
    MOVF num1,0
    MOVWF aux
    MOVF num2,0
    MOVWF num1
    MOVF aux,0
    MOVWF num2
    RETURN
    
    
RESTA
   MOVF num1,0
   SUBWF num2,0
   MOVWF diferencia
   RETURN
;--------------------------
   
IGUAL2
   nop
   return
mayor
   ;MOVLW .6
   ;MOVWF 0X28
   DECFSZ diferencia,1   
   goto GiroMay
   GOTO LEER
GiroMay
    MOVLW   B'00000001'	; inicio sentido manecillas del reloj
    MOVWF   PORTB
    CALL    RETARDO
    MOVLW   B'00000010'	;
    MOVWF   PORTB
    CALL    RETARDO
    MOVLW   B'00000100'	;
    MOVWF   PORTB
    CALL    RETARDO
    MOVLW	B'00001000'	;
    MOVWF   PORTB
    CALL    RETARDO
    MOVLW	B'00010000'	;
    MOVWF   PORTB
    CALL    RETARDO
    MOVLW	B'00100000'	;
    MOVWF   PORTB
    CALL    RETARDO
    goto mayor
   
   
TABLA1;
    ADDWF PCL,1
    RETLW   B'10000001'
    RETLW   B'00000010'
    RETLW   B'00000100'
    RETLW   B'00001000'
    RETLW   B'00010000'
    RETLW   B'00100000'
    MOVWF PORTB
    GOTO mayor
    




;-----------Rutina de Retardo-----------


DECCC
    MOVLW 0XFF
    MOVWF 0X23
DECC
    MOVLW 0XFF
    MOVWF 0X22
RETARDO
    DECFSZ 0X22,1
    GOTO RETARDO
    DECFSZ 0X23,1
    GOTO DECC     

    END